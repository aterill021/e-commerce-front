import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { PrincipalComponent } from './componentes/contenedores/principal/principal.component';
import { ContenedorCarritoComponent } from './componentes/contenedores/contenedor-carrito/contenedor-carrito.component';

export const routes: Routes = [
    { path: 'carrito', component: ContenedorCarritoComponent },
    {path: 'rappi', component: PrincipalComponent },
    {path: '', redirectTo: 'rappi', pathMatch: 'full'}
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
