import { Component, OnInit } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { ConsultasService } from 'src/app/servicios/api/consultas.service';
import { CategoriasProductosService } from 'src/app/servicios/transformadores/categorias-productos.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-categorias-productos',
  templateUrl: './categorias-productos.component.html',
  styleUrls: ['./categorias-productos.component.css']
})
export class CategoriasProductosComponent implements OnInit {

  public $categoriasMenu: Observable<Array<Categoria>>;

  constructor(private consultas: ConsultasService,
    private categoriasTransf: CategoriasProductosService) { }

  ngOnInit() {
    this.$categoriasMenu = forkJoin(
      this.consultas.consultarCategorias(),
      this.consultas.consultarProductos()
    )
    .pipe(
        map( ([categorias, productos]) => {
          return this.categoriasTransf.construirCategoriasMenu(categorias, productos);
        })
    );

  }

}
