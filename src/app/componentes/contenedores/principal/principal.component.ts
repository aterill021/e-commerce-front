import { Component, OnInit } from '@angular/core';
import { ConsultasService } from '../../../servicios/api/consultas.service';
import {forkJoin} from 'rxjs';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { CategoriasProductosService } from '../../../servicios/transformadores/categorias-productos.service';
import { CarritoService } from 'src/app/servicios/carrito-compras/carrito.service';


@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.scss']
})
export class PrincipalComponent implements OnInit {

  public $categoriasMenu: Observable<Array<Categoria>>;
  public categoriaSeleccionada: Categoria;

  constructor(private consultas: ConsultasService,
     private categoriasTransf: CategoriasProductosService,
     public servicioCarrito: CarritoService) { }

  ngOnInit() {
    this.$categoriasMenu = forkJoin(
      this.consultas.consultarCategorias(),
      this.consultas.consultarProductos()
    )
    .pipe(
        map( ([categorias, productos]) => {
          return this.categoriasTransf.construirCategoriasMenu(categorias, productos);
        })
    );
  }

  seleccionarCategoria(categoria: Categoria) {
    this.categoriaSeleccionada = (categoria.products) ? categoria : this.categoriaSeleccionada;
  }

}
