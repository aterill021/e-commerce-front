import { Component, OnInit } from '@angular/core';
import { CarritoService } from 'src/app/servicios/carrito-compras/carrito.service';

@Component({
  selector: 'app-contenedor-carrito',
  templateUrl: './contenedor-carrito.component.html',
  styleUrls: ['./contenedor-carrito.component.css']
})
export class ContenedorCarritoComponent implements OnInit {

  productos: Array<Producto>;
  constructor(private carritoService: CarritoService) {}

  ngOnInit() {
    this.productos = this.carritoService.listaProductosCarrito;
  }

  onEliminarProducto(producto) {
    this.productos = this.carritoService.retirarProducto(producto);
  }

}
