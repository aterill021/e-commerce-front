import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrincipalComponent } from './contenedores/principal/principal.component';
import { ServiciosModule } from '../servicios/servicios.module';
import { MenuCategoriasComponent } from './presentacion/menu-categorias/menu-categorias.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PanelMenuModule} from 'primeng/panelmenu';
import { ListaProductosComponent } from './presentacion/lista-productos/lista-productos.component';
import {DataViewModule} from 'primeng/dataview';
import {DropdownModule} from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import {PanelModule} from 'primeng/panel';
import {ButtonModule} from 'primeng/button';
import {ToolbarModule} from 'primeng/toolbar';
import { DialogModule } from 'primeng/dialog';
import { CategoriasProductosComponent } from './contenedores/categorias-productos/categorias-productos.component';
import { ContenedorCarritoComponent } from './contenedores/contenedor-carrito/contenedor-carrito.component';
import {InputTextModule} from 'primeng/inputtext';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ServiciosModule.forRoot(),
    BrowserAnimationsModule,
    PanelMenuModule,
    DataViewModule,
    DropdownModule,
    PanelModule,
    ButtonModule,
    ToolbarModule,
    DialogModule,
    InputTextModule
  ],
  declarations: [
    PrincipalComponent,
    MenuCategoriasComponent,
    ListaProductosComponent,
    CategoriasProductosComponent,
    ContenedorCarritoComponent,
  ],
  exports: [
    PrincipalComponent,
    MenuCategoriasComponent,
    CategoriasProductosComponent,
    ContenedorCarritoComponent]
})
export class ComponentesModule { }
