import { Component, OnInit, Input } from '@angular/core';
import { categories } from '../../../servicios/api/categories';
import {MenuItem} from 'primeng/api';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-menu-categorias',
  templateUrl: './menu-categorias.component.html',
  styleUrls: ['./menu-categorias.component.scss']
})
export class MenuCategoriasComponent implements OnInit {

  @Input() categorias: Array<Categoria>;
  @Output() categoriaClicked: EventEmitter<Categoria> = new EventEmitter();
  items: MenuItem[];
  constructor(public app: AppComponent) { }

  cargarCategoriasEnMenu(categorias: Array<Categoria>): Array<MenuItem> {
     return categorias.map(this.cargarCategoriaMenu.bind(this));
  }

  cargarCategoriaMenu(categoria: Categoria): any {
      const items = categoria.sublevels;
      return {
          label: categoria.name,
          items: (categoria.sublevels) ? categoria.sublevels.map(this.cargarCategoriaMenu.bind(this)) : [],
          command: this.onItemClick.bind(this),
          categoria: categoria
      };
  }

  ngOnInit() {

    this.items = this.cargarCategoriasEnMenu(this.categorias);
  }

  onItemClick(item) {
    this.categoriaClicked.emit(item.item.categoria);
  }

}
