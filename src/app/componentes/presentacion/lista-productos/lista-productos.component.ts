import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { ProductoCarrito } from 'src/app/modelos/producto-carrito.model';

@Component({
  selector: 'app-lista-productos',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.scss']
})
export class ListaProductosComponent implements OnInit {

  @Input() productos: Array<ProductoCarrito>;
  @Input() carrito: boolean;
  @Output() agregarProducto: EventEmitter<Producto> = new EventEmitter();
  sortOptions: SelectItem[];
  @Output() eliminarProducto: EventEmitter<Producto> = new EventEmitter();


    sortKey: string;

    filterField: string;
    sortOrder: 1;

    productoSeleccionado: ProductoCarrito;
    filtrosOptions;

    displayDialog: boolean;

  constructor() { }

  ngOnInit() {
    this.sortOptions = [
      {label: 'precio', value: 'price'},
      {label: 'Disponibilidad', value: 'available'},
      {label: 'Cantidad stock', value: 'quantity'}
    ];

    this.filtrosOptions = [
      {label: 'precio', value: 'price'},
      {label: 'disponibles', value: 'available'},
      {label: 'Cantidad stock', value: 'quantity'}
    ];
  }

  onSortChange(event) {
    this.sortKey = event.value;
  }

  onFilterChange(event) {
    this.filterField = event.value;
  }

  seleccionarProducto(producto) {
    this.productoSeleccionado = producto;
    this.displayDialog = true;
  }

onDialogHide() {
    this.productoSeleccionado = null;
}

onAgregarProducto(producto) {
  this.displayDialog = false;
  this.agregarProducto.emit(producto);
}

onEliminarProducto(producto) {
  this.eliminarProducto.emit(producto);
}

}
