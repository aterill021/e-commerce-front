import { TestBed, inject } from '@angular/core/testing';

import { CategoriasProductosService } from './categorias-productos.service';

describe('CategoriasProductosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CategoriasProductosService]
    });
  });

  it('should be created', inject([CategoriasProductosService], (service: CategoriasProductosService) => {
    expect(service).toBeTruthy();
  }));
});
