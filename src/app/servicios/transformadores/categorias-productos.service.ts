import { Injectable } from '@angular/core';

@Injectable()
export class CategoriasProductosService {

  constructor() { }

  construirCategoriasMenu(categorias: Array<Categoria>, productos: Array<Producto> ): Array<Categoria> {

    return categorias.map( (categoria) => {
      if (categoria.sublevels && categoria.sublevels.length > 0) {
        this.construirCategoriasMenu(categoria.sublevels, productos);
      } else {
        categoria.products = productos.filter(producto => {
          return producto.sublevel_id === categoria.id;
        });
      }
      return categoria;
    } );
  }

}
