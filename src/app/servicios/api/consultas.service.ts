import { Injectable } from '@angular/core';
// import { of } from 'rxjs';
import { Observable } from 'rxjs';

import {categories} from './categories';
import {products} from './products';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable()
export class ConsultasService {

  constructor() { }

  consultarCategorias(): Observable<Array<Categoria>> {
    return of<Array<Categoria>>(categories.categories);
  }

  consultarProductos(): Observable<Array<Producto>> {
    return of(products.products)
    .pipe(
      map( (productos) => {
        return productos.map(producto => ({... producto, price: Number.parseFloat( producto.price.replace('$', '').replace(',', ''))}));
      })
    );
  }


}
