import { NgModule, ModuleWithProviders, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultasService } from './api/consultas.service';
import { CategoriasProductosService } from './transformadores/categorias-productos.service';
import { CarritoService } from './carrito-compras/carrito.service';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class ServiciosModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ServiciosModule,
      providers: [
        ConsultasService,
        CategoriasProductosService,
        CarritoService,
        // {
        //   provide: APP_INITIALIZER,
        //   useFactory: configFactory,
        //   deps: [IndexedDbService],
        //   multi: true
        // }
      ]
    };
  }
}
