import { Injectable } from '@angular/core';
import { NgxCookieRService } from '../persistence/ngx-cookie-r.service';

@Injectable({
  providedIn: 'root'
})
export class CarritoService {

  private listaProductos: Array<Producto> = [];
  constructor(public persistence: NgxCookieRService) {
    this.listaProductos = persistence.getListaProductos();
   }

  public agregarProducto(producto: Producto) {
    this.listaProductos.push(producto);
    this.persistence.guardarListaProductos(this.listaProductos);
  }

  public retirarProducto(producto: Producto): Array<Producto> {
    this.listaProductos = this.listaProductos.filter(productoLista => productoLista.id !== producto.id);
    this.persistence.guardarListaProductos(this.listaProductos);
    return this.listaProductos;
  }

  get listaProductosCarrito() {
    return this.listaProductos;
  }
}
