import { TestBed, inject } from '@angular/core/testing';

import { NgxCookieRService } from './ngx-cookie-r.service';

describe('NgxCookieRService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NgxCookieRService]
    });
  });

  it('should be created', inject([NgxCookieRService], (service: NgxCookieRService) => {
    expect(service).toBeTruthy();
  }));
});
