import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class NgxCookieRService {

  constructor(private cookieService: CookieService) { }

  productos = 'productos';

  guardarListaProductos(lista: Array<Producto>) {
    this.cookieService.set(this.productos, JSON.stringify(lista));
  }

  getListaProductos() {
    const productos = this.cookieService.get(this.productos);
    return productos.length > 0 ? JSON.parse(this.cookieService.get(this.productos)) : [];
  }
}
