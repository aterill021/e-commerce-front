interface Producto {
  quantity;
  price: number;
  available;
  sublevel_id;
  name;
  id;
}
