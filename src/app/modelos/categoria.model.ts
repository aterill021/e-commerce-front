interface Categoria {
  id: number;
  name: string;
  sublevels?: Categoria[];
  products?: Array<Producto>;
}
